package com.np.service;

import java.util.List;

import com.np.dto.UserDTO;
import com.np.exception.ServiceException;
import com.np.model.entity.User;


public interface IUserService {

	public boolean saveUser(final UserDTO user) throws ServiceException;
	public boolean updateUser(final UserDTO user) throws ServiceException;
	public User getUserbyUserId(final Long id) throws ServiceException;
	public User getUserbyUserName(final String userName) throws ServiceException;
	public List<User> getAllUser() throws ServiceException;
}

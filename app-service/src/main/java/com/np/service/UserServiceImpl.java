package com.np.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.np.dao.user.IUserDao;
import com.np.dto.UserDTO;
import com.np.exception.DaoException;
import com.np.exception.ServiceException;
import com.np.model.entity.User;

public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserDao userDao;

	public IUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public boolean saveUser(final UserDTO user) throws ServiceException  {
		boolean status = false;
		if (user != null) {
			try {

				if (user.getUserId() == null || user.getPassword() == null)
					return false;
				User newUser = new User();
				newUser.setUserId(user.getUserId());
				newUser.setPassword(user.getPassword());
				//newUser.setCreateBy("ASD");
				//newUser.setLastUpdBy("ASD");
				newUser.setCreateDt(new Date());
				status =  userDao.saveObject(newUser);
			} catch (DaoException e) {
				throw new ServiceException("Error In Save Error User Id :: " + user.getUserId() , e);
			}
			return status;
		}

		return false;
	}

	@Override
	public boolean updateUser(final UserDTO user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public User getUserbyUserId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserbyUserName(String userName) throws  ServiceException {
		
		try {
			return userDao.getUserByUserName(userName);
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new ServiceException("Exception Occured at Service level");
		}
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		return null;
	}

}

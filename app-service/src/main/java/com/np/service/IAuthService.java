package com.np.service;

import java.util.List;

import com.np.dto.CustomerDTO;
import com.np.exception.ServiceException;


public interface IAuthService {

	public List<CustomerDTO> getcustomerbyUserName(final CustomerDTO customerDTO) throws ServiceException;
}

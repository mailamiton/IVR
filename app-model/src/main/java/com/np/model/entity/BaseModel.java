package com.np.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseModel {
	
	@Column(name="created_by", nullable=false)
	private String createBy;
	@Column(name="created_dt", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable=true)
	private Date createDt;
	@Column(name="last_upd_by", nullable=false)
	private String lastUpdBy;
	@Column(name="last_upd_dt", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable=false)
	private Date lastUpdDt;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private boolean status;
	
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(final String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(final Date createDt) {
		this.createDt = createDt;
	}
	public String getLastUpdBy() {
		return lastUpdBy;
	}
	public void setLastUpdBy(final String lastUpdBy) {
		this.lastUpdBy = lastUpdBy;
	}
	public Date getLastUpdDt() {
		return lastUpdDt;
	}
	public void setLastUpdDt(final Date lastUpdDt) {
		this.lastUpdDt = lastUpdDt;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(final boolean status) {
		this.status = status;
	}
	
}
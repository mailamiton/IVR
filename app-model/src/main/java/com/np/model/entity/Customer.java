package com.np.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="e_customer")
public class Customer extends BaseModel {

    @Column(name="member_id", unique=true, nullable=false)
    private String memberId;
     
    @ManyToOne
    @JoinColumn(name="prmy_address", nullable=true)
    private PhoneNumbers phoneNumbers;


}

package com.np.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="e_phones")
public class PhoneNumbers extends BaseModel {

    @Column(name="phone", unique=true, nullable=false)
    private String phone;
    
     
}

package com.np.model.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="e_user_address")
public class UserAddress extends BaseModel{
	
	@ManyToOne
	@JoinColumn(name="addrs_id")
	Address address;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	User user;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
 }

package com.np.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="e_user")
public class User extends BaseModel {

    @Column(name="user_id", unique=true, nullable=false)
    private String userId;
     
    @Column(name="password", nullable=false)
    private String password;

    @ManyToOne
    @JoinColumn(name="prmy_address", nullable=true)
    private Address address;

	public String getUserId() {
		return userId;
	}

	public void setUserId(final String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

}

package com.np.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="e_address")
public class Address extends BaseModel {

	@Column(name="addr_name", nullable=false)
	private String addrName;
	
	@Column(name="addr_line1")
	private String addrline1;

	@Column(name="addr_line2")
	private String addrline2;

	@Column(name="addr_line3")
	private String addrline3;
	
	@Column(name="pincode")
	private String pinCode;
	
	@Column(name="city")
	private String city;

	@Column(name="state")
	private String state;
	
	@Column(name="phone1")
	private String phone1;
	
	@Column(name="phone2")
	private String phone2;
	
	@Column(name="phone3")
	private String phone3;
	
   	public String getAddrName() {
		return addrName;
	}

	public void setAddrName(final String addrName) {
		this.addrName = addrName;
	}

	public String getAddrline1() {
		return addrline1;
	}

	public void setAddrline1(final String addrline1) {
		this.addrline1 = addrline1;
	}

	public String getAddrline2() {
		return addrline2;
	}

	public void setAddrline2(final String addrline2) {
		this.addrline2 = addrline2;
	}

	public String getAddrline3() {
		return addrline3;
	}

	public void setAddrline3(final String addrline3) {
		this.addrline3 = addrline3;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(final String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(final String state) {
		this.state = state;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(final String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(final String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(final String phone3) {
		this.phone3 = phone3;
	}

 }

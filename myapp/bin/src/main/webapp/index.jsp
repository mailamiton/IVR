<html lang="en">
<head>
<title>AngularJS Routing example</title>
</head>
<body ng-app="myApp">
	<script
		src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
	<script
		src="http://localhost:8082/myapp/resources/js/app.js"></script>
    <script
		src="http://localhost:8082/myapp/resources/js/controller/login/login.js"></script>
	<script
		src="http://localhost:8082/myapp/resources/js/factory/myappFactory.js"></script>
<div ng-view></div>
</body>
</html>
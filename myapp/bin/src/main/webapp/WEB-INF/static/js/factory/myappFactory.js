myApp.factory('myAppService', myAppService);
function myAppService($http){
	return {
        PostCall : function(data, url, successCallback, errorCallback) {
            $http({
                url : url,
                method : 'POST',
                data : JSON.stringify(data),
                headers : {
                    'Content-Type' : 'application/json'
                }
            }).success(successCallback).error(errorCallback);
        }
	}
	return {
        GetCall : function(url, successCallback, errorCallback) {
            $http({
                url : url,
                method : 'GET',
            }).success(successCallback).error(errorCallback);
        }
	}
}
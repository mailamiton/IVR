"use strict";
var myApp = angular.module('myApp', []);

myApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'resources/partials/login/login.html',
		controller : 'LoginController'
	}).when('/register', {
		templateUrl : 'resources/partials/register/register.html',
		controller : 'RegisterController'
	}).when('/home', {
		templateUrl : 'resources/partials/home/home.html'
	}).otherwise({
		redirectTo : '/'
	});
} ]);
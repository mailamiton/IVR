package com.np.ecanteen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.np.dto.UserDTO;
import com.np.exception.DaoException;
import com.np.exception.ServiceException;
import com.np.model.entity.User;
import com.np.service.IUserService;

@Controller
public class userController {

	@Autowired
	private IUserService userService;

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "/getUserByUserName", method = RequestMethod.POST)
	public ResponseEntity<User> getUserByUserName(@RequestBody UserDTO user) {
		User userResp = null;
		try {
			userResp = userService.getUserbyUserName(user.getUserId());
			return new ResponseEntity<User>(userResp, HttpStatus.OK);
		} catch (ServiceException e) {
			//e.printStackTrace();
			e.handleException();
			return new ResponseEntity<User>(userResp,
					HttpStatus.EXPECTATION_FAILED);
			
		} 
	}

	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createUser(@RequestBody UserDTO user) {
		try {
			return new ResponseEntity<Boolean>(userService.saveUser(user),
					HttpStatus.OK);
		} catch (ServiceException e) {
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false,
					HttpStatus.EXPECTATION_FAILED);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false,
					HttpStatus.EXPECTATION_FAILED);
		}
	}
}

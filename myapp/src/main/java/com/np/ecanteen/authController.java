package com.np.ecanteen;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.np.dto.CustomerDTO;
import com.np.exception.ServiceException;
import com.np.service.IAuthService;

@Controller
public class authController {

	@Autowired
	private IAuthService authService;

	public IAuthService getAuthService() {
		return authService;
	}

	public void setAuthService(IAuthService authService) {
		this.authService = authService;
	}

	@RequestMapping(value = "/getcustomerByUserName", method = RequestMethod.POST)
	@ExceptionHandler(Exception.class)
	public ResponseEntity<List<CustomerDTO>> getcustomerByUserName(
			@RequestBody CustomerDTO customerDTO) {
		List<CustomerDTO> customerResp = null;
		try {
			if (customerDTO != null) {
				customerResp = authService.getcustomerbyUserName(customerDTO);
			}

			return new ResponseEntity<List<CustomerDTO>>(customerResp, HttpStatus.OK);
		} catch (ServiceException e) {
			// e.printStackTrace();
			e.handleException();


		}
		return null;
	}
}

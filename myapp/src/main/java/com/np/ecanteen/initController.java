package com.np.ecanteen;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class initController {

	 @RequestMapping(value="/demo", method = RequestMethod.GET)
	   public String demo(ModelMap model) {
	      model.addAttribute("message", "Hello Spring MVC Framework!");
	      return "home/demo";
	   }

}

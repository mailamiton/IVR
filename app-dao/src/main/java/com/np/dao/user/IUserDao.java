package com.np.dao.user;

import com.np.dao.common.ICrud;
import com.np.exception.DaoException;
import com.np.model.entity.BaseModel;
import com.np.model.entity.User;

public interface IUserDao extends ICrud<BaseModel> {
	
	public User getUserByUserName(final String userName) throws DaoException;

}

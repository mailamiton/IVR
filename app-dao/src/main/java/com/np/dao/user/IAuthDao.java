package com.np.dao.user;

import java.util.List;

import com.np.dao.common.ICrud;
import com.np.exception.DaoException;
import com.np.model.entity.BaseModel;
import com.np.model.entity.Customer;

public interface IAuthDao extends ICrud<BaseModel> {
	
	public List<Customer> getCustomerByMobileNumber(final String mobileNumber) throws DaoException;

}

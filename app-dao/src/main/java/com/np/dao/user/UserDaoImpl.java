package com.np.dao.user;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.np.dao.common.ICrud;
import com.np.exception.DaoException;
import com.np.model.entity.BaseModel;
import com.np.model.entity.User;

public class UserDaoImpl<T extends BaseModel> implements IUserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private ICrud commonDao;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public ICrud getCommonDao() {
		return commonDao;
	}

	public void setCommonDao(ICrud commonDao) {
		this.commonDao = commonDao;
	}

	@Override
	public boolean saveObject(Object object) throws DaoException {
		return commonDao.saveObject(object);
	}

	@Override
	public boolean udateObject(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteObject(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object getObject(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BaseModel> getAllObject(Class<BaseModel> classVal) {
		// TODO Auto-generated method stub
		return null;
	}

	public User getUserByUserName(final String userName) throws DaoException {
		User user = null;
		try{
		Session session = getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("userId", userName));
		user = (User) criteria.uniqueResult();
		 System.out.println("user is " + user.getPassword());
		} catch (Exception e){
			throw new DaoException("Exception Occured at DAO Level");
		}
		return user;
	}

}

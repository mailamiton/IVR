package com.np.dao.common;

import java.util.List;

import com.np.exception.DaoException;
import com.np.model.entity.BaseModel;


public interface ICrud<T extends BaseModel> {

	public boolean saveObject(final Object object) throws DaoException;
	
	public boolean udateObject(final Object object) throws DaoException;
	
	public boolean deleteObject(final Object object) throws DaoException;
	
	public Object getObject(final long id) throws DaoException;
	
	public List<T> getAllObject(final Class<T> classVal) throws DaoException;

}

package com.np.dao.common;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.np.model.entity.BaseModel;
import com.np.model.entity.User;

public class CrudImpl<T extends BaseModel> implements ICrud<T> {
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public boolean saveObject(final Object object) {
		Session session = getSessionFactory().openSession();
		try {
			Transaction tx = session.beginTransaction();
			session.save(object);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	@Override
	public boolean udateObject(final Object object) {
		Session session = getSessionFactory().openSession();
		try {
			Transaction tx = session.beginTransaction();
			session.update(object);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		return true;
	}

	@Override
	public boolean deleteObject(final Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<T> getAllObject(final Class<T> classVal) {
		Session session = getSessionFactory().openSession();
		List<T> listObjects = null;
		try {
			listObjects = session.createCriteria(classVal).list();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return listObjects;
	}

	@Override
	public Object getObject(final long id) {
		Session session = getSessionFactory().openSession();
		Object object = null;
		try {
			object = session.get(User.class, id);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return object;
		
	}

}

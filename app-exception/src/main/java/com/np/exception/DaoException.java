package com.np.exception;

public class DaoException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DaoException() {
		super();
	}

	public DaoException(String exceptionMessage, Exception exception) {
		super(exceptionMessage, exception);
	}

	public DaoException(String exceptionMessage) {
		super(exceptionMessage);
	}
	
	

}

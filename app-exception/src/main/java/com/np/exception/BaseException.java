package com.np.exception;

public class BaseException extends Exception {

	private static final long serialVersionUID = 1L;
	private final String exceptionMessage;

	public BaseException(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
		System.out.println(exceptionMessage);
	}

	public BaseException() {
		this.exceptionMessage = "";
	}

	public BaseException(String exceptionMessage, Exception exception) {
		this.initCause(exception);
		this.exceptionMessage = exceptionMessage;
	}

	public void setCause(final Throwable e) {
		this.initCause(e);
	}

	public String getMessage() {
		return exceptionMessage;
	}
	
	public String handleException() {
		System.out.println("From Handle Exception :: " + this.exceptionMessage);
		return exceptionMessage;
	}

}

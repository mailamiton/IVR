package com.np.dto;


public class CustomerDTO {

    private String memberId;
    private String phoneNumber;

	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(final String memberId) {
		this.memberId = memberId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
    
    

	
}
